class CreateExchanges < ActiveRecord::Migration[5.2]
  def change
    create_table :exchanges, id: :uuid do |t|
      t.string :name
      t.string :country

      t.timestamps
    end
  end
end
