class AddCodeToExchanges < ActiveRecord::Migration[5.2]
  def change
    add_column :exchanges, :code, :string
    add_column :exchanges, :tz, :string
  end
end
