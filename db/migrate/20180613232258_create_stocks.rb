class CreateStocks < ActiveRecord::Migration[5.2]
  def change
    create_table :stocks, id: :uuid do |t|
      t.string :name
      t.string :ticker
      t.references :exchange, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
