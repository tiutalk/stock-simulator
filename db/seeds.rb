require 'net/http'
require 'parallel'

if Exchange.count.zero?
  Exchange.create!(name: 'BM&F Bovespa', code: 'B3', tz: 'BRST', country: 'BR')
  Exchange.create!(name: 'NASDAQ', code: 'NASDAQ', tz: 'EST', country: 'US')
end

@b3 = Exchange.b3
@nasdaq = Exchange.nasdaq
@pool_size = ActiveRecord::Base.connection_pool.size

if @b3.present? && @b3.stocks.empty?
  puts "\r\n=> Fetching #{@b3.name} data from UOL..."
  url = "http://cotacoes.economia.uol.com.br/acoes-bovespa.html?exchangeCode=.BVSP&page=1&size=2000"
  body = Net::HTTP.get(URI.parse(url))
  doc = Nokogiri::HTML(body)
  rows = doc.xpath('//*[@id="resultado-busca"]/ul/li')

  Parallel.each(rows[1..-1], in_threads: @pool_size, progress: "Importing #{@b3.code} symbols") do |row|
    ActiveRecord::Base.connection_pool.with_connection do
      name, ticker = row.css('span').map(&:text).map(&:strip)
      Stock.create!(exchange: @b3, name: name, ticker: ticker)
    end
  end
end

if @nasdaq.present? && @nasdaq.stocks.empty?
  puts "\r\n=> Fetching #{@nasdaq.name} data from IEX..."
  url = "https://api.iextrading.com/1.0/ref-data/symbols?filter=symbol,name,isEnabled,type"
  body = Net::HTTP.get(URI.parse(url))
  rows = JSON.parse(body)

  Parallel.each(rows, in_threads: @pool_size, progress: "Importing #{@nasdaq.code} symbols") do |row|
    next unless row['isEnabled'] && row['type'] == 'cs'

    ActiveRecord::Base.connection_pool.with_connection do
      name, ticker = row.slice(*%w(name symbol)).values
      Stock.create!(exchange: @nasdaq, name: name, ticker: ticker)
    end
  end
end

