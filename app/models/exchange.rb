class Exchange < ApplicationRecord
  # Scopes
  scope :b3, -> { find_by(code: 'B3') }
  scope :nasdaq, -> { find_by(code: 'NASDAQ') }

  # Associations
  has_many :stocks, inverse_of: :exchange

  # Validations
  validates :name, :code, :country, presence: true
end
