class Stock < ApplicationRecord
  # Associations
  belongs_to :exchange, inverse_of: :stocks

  # Validations
  validates :name, :ticker, :exchange, presence: true
end
