require 'rails_helper'

RSpec.describe Exchange, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:stocks).inverse_of(:exchange) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:code) }
    it { is_expected.to validate_presence_of(:country) }
  end
end
